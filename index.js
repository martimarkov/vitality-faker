const config = require("config");
const request = require("request-promise");
const email = require("emailjs");

// min and max included
function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function deepEquals(o1, o2) {
  var k1 = Object.keys(o1).sort();
  var k2 = Object.keys(o2).sort();
  if (k1.length != k2.length) return false;
  return k1
    .zip(k2, function(keyPair) {
      if ((typeof o1[keyPair[0]] == typeof o2[keyPair[1]]) == "object") {
        return deepEquals(o1[keyPair[0]], o2[keyPair[1]]);
      } else {
        return o1[keyPair[0]] == o2[keyPair[1]];
      }
    })
    .all();
}

const stepsPerHour = [
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0, //10:00
  0, //11:00
  0, //12:00
  0, //13:00
  0, //14:00
  0, //15:00
  0, //16:00
  0, //17:00
  0, //18:00
  0, //19:00
  0, //20:00
  0, //21:00
  0, //22:00
  0, //23:00
];

const stepsPerHourWeight = [
  0, // 00:00
  0, // 01:00
  0, // 02:00
  0, // 03:00
  0, // 04:00
  0, // 05:00
  0, // 06:00
  0.37, // 07:00
  1.5, // 08:00
  0.9, // 09:00
  0.34, // 10:00
  0.45, // 11:00
  0.65, // 12:00
  0.8, // 13:00
  0.3, // 14:00
  0.4, // 15:00
  0.2, // 16:00
  0.6, // 17:00
  0.8, // 18:00
  1, // 19:00
  1, // 20:00
  1, // 21:00
  0.4, // 22:00
  0, // 23:00
];

Object.prototype.deepEquals = deepEquals;
const server = email.server.connect(config.get("app.email.server"));
let emailData = null;

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
let timeNow = new Date();
let timeBefore = new Date();
let timeout = 0;
let intervalRef = null;

const hourCron = async () => {
  clearInterval(intervalRef);

  const randomDelay = randomIntFromInterval(-1000 * 60 * 4, 1000 * 60 * 5);
  console.log(`Random delay will be ${randomDelay / 1000 / 60} seconds`);
  timeout = 1000 * 60 * 60 + randomDelay;
  intervalRef = setInterval(hourCron, timeout);

  timeNow = new Date();
  const step = randomIntFromInterval(
    config.get("app.lowerBound"),
    config.get("app.upperBound"),
  );
  const currentHour = timeNow.getHours();

  stepsPerHour[currentHour] = Math.floor(
    step * stepsPerHourWeight[currentHour],
  );
  const steps = stepsPerHour.reduce((a, b) => a + b, 0);

  console.log(`Hour ${currentHour}: ${stepsPerHour[currentHour]} -> ${steps}`);

  stepsPerHour.map((item, index) => {
    console.log(`Hour ${index} -> ${item}`);
  });
  if (steps > 0) {
    await addNewSteps();
  }
  if (timeBefore.getDate() !== timeNow.getDate()) {
    console.log("Switching day...");
    console.log(timeBefore.getDate());
    console.log(timeNow.getDate());
    for (let index = 0; index < stepsPerHour.length; index++) {
      stepsPerHour[index] = 0;
    }
  }
  timeBefore.setDate(timeNow.getDate());
};

const addNewSteps = async () => {
  const steps = stepsPerHour.reduce((a, b) => a + b, 0);
  if (steps === 0) {
    return;
  }
  const token = "bb5840c0-8a95-42df-834d-7446710673a3";
  const today = new Date();
  today.setMilliseconds(0);
  const tomorrow = new Date();
  tomorrow.setDate(today.getDate() + 1);

  const todayDateString = `${today.getFullYear()}-${(
    "0" +
    (today.getMonth() + 1)
  ).slice(-2)}-${("0" + today.getDate()).slice(-2)}`;
  const tomorrowDateString = `${tomorrow.getFullYear()}-${(
    "0" +
    (tomorrow.getMonth() + 1)
  ).slice(-2)}-${("0" + tomorrow.getDate()).slice(-2)}`;
  const todayTimeString = `${("0" + today.getHours()).slice(-2)}:${(
    "0" + today.getMinutes()
  ).slice(-2)}:${("0" + today.getSeconds()).slice(-2)}`;
  const todayDateStringShort = `${today.getFullYear()}${(
    "0" +
    (today.getMonth() + 1)
  ).slice(-2)}${("0" + today.getDate()).slice(-2)}`;

  const jsonData = {
    readings: [
      {
        manufacturer: config.get("app.device.manufacturer"),
        endTime: `${tomorrowDateString}T00:00:00+00:00`,
        integrity: "VERIFIED",
        partnerReadingId: `WALK_${config.get(
          "vitality.memberId",
        )}_${todayDateStringShort}_00:00:00`,
        model: config.get("app.device.model"),
        dataCategory: "ROUTINE",
        readingType: "WALK",
        startTime: `${todayDateString}T00:00:00+00:00`,
        workout: { totalSteps: steps },
      },
    ],
    device: config.get("app.device"),
    header: {
      partnerSystem: config.get("vitality.partnerSystem"),
      user: {
        entityNo: `${config.get("vitality.memberId")}`,
      },
      rawUploadData: "No Raw Data",
      tenantId: `${config.get("vitality.tenantId")}`,
      sessionId: `${config.get(
        "vitality.memberId",
      )}_${todayDateString}T${todayTimeString}+0000`,
      verified: "true",
      uploadDate: `${todayDateString}T${todayTimeString}+00:00`,
    },
  };

  const opts = {
    method: "POST",
    uri: config.get("vitality.uploadUrl"),
    gzip: true,
    headers: {
      Authorization: `Bearer ${token}`,
      "User-Agent": config.get("app.userAgent"),
      Host: config.get("vitality.uploadHost"),
      "X-Vsl-Timestamp": today.valueOf() / 1000,
      "X-Vsl-Sourcetype": "CORPORATE",
      "Accept-Encoding": "deflate, gzip",
      "Accept-Language": "en;q=1, bg;q=0.9",
    },
    json: jsonData,
  };
  const response = await request(opts).catch((error) => {
    // console.log(error);
    emailData = {
      text: `Error name:\n\n${error.name}\n\n\nError message:\n\n${
        error.message
      }\n\n\nRequest options:\n\n${JSON.stringify(error.options)}`,
      from: config.get("app.email.from"),
      to: config.get("app.email.to"),
      subject: "[VITALITY] - Server POST error",
    };
  });

  // console.log(response);

  if (response) {
    const expectedResponse = config.get(
      "vitality.expectedResponse.response.reason",
    );
    const expectedSuccess = config.get(
      "vitality.expectedResponse.response.success",
    );

    try {
      if (
        response.response.success !== expectedSuccess ||
        response.response.reason !== expectedResponse
      ) {
        throw (new Error().name = "UnexpectedResponse");
      }
    } catch (error) {
      if (error.name === "SyntaxError") {
        emailData = {
          text: `Here is the reponse ${response}`,
          from: config.get("app.email.from"),
          to: config.get("app.email.to"),
          subject: "[VITALITY] - Non JSON response",
        };
      }

      if (error.name === "UnexpectedResponse") {
        emailData = {
          text: `Here is the reponse:\n\n\n${response}`,
          from: config.get("app.email.from"),
          to: config.get("app.email.to"),
          subject: "[VITALITY] - Unexpected response",
        };
      }
    }
  }
  if (emailData) {
    console.log("Error encountered. Sending Notification...");
    const pr = new Promise(function(resolve, reject) {
      server.send(emailData, function(err, message) {
        if (err) {
          reject(err);
        }
        resolve(message);
      });
    });

    const resp = await pr;
    // console.log(resp);

    process.exit(1);
  }
  console.log(response);
};

(async () => {
  var argv = require("minimist")(process.argv.slice(2));
  if (argv.hasOwnProperty("init")) {
    console.log(`Will init the first day with: ${argv["init"]}`);
    const arr = argv["init"].split(",").map((item) => parseInt(item));
    const arrLength = arr.length;
    if (arrLength !== 24) {
      console.log("Incompatible array initialaser.");
      process.exit(2);
    }
    for (let i = 0; i < arrLength; i++) {
      stepsPerHour[i] = arr[i];
    }
  }
  intervalRef = setInterval(hourCron, timeout);
})();
